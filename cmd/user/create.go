package user

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/logrusorgru/aurora/v3"
	"github.com/urfave/cli/v2"
	"gitlab.com/1f320/worldbase/db"
	"gitlab.com/1f320/worldbase/file"
	"golang.org/x/term"
)

var createUserCommand = &cli.Command{
	Name:   "create",
	Usage:  "Create a user",
	Action: runCreate,
}

var usernameRe = regexp.MustCompile(`^[\w-]{1,32}$`)

func runCreate(c *cli.Context) (err error) {
	w := writer{c.App.Writer}

	// silence log output
	log.SetOutput(io.Discard)

	dbName := c.String("database")

	f, err := file.Open(dbName)
	if err != nil {
		return cli.Exit(fmt.Sprintf("Error opening database: %v", err), 1)
	}

	w.write(aurora.Bold("Please enter a username: "))

	s := bufio.NewScanner(os.Stdin)

	if !s.Scan() {
		return cli.Exit(s.Err(), 1)
	}

	name := strings.TrimSpace(s.Text())
	if len(name) > 32 {
		return cli.Exit("Username may not be longer than 32 characters.", 1)
	}
	if name == "" {
		return cli.Exit("Username may not be empty.", 1)
	}
	if !usernameRe.MatchString(name) {
		return cli.Exit(
			"Username may only contain alphanumeric characters, underscores, and dashes.", 1)
	}

	w.write(aurora.Bold("Please enter a password: "))

	fd := int(os.Stdin.Fd())

	pass, err := term.ReadPassword(fd)
	if err != nil {
		return cli.Exit(
			fmt.Sprintf("\nError reading password: %v", err), 1)
	}

	// just enforce length
	if len(pass) < 8 || len(pass) > 128 {
		return cli.Exit(
			"\nPassword must be between 8 and 128 characters long.", 1)
	}

	w.write(aurora.Bold("\nRepeat your password: "))

	pass2, err := term.ReadPassword(fd)
	if err != nil {
		return cli.Exit(
			fmt.Sprintf("\nError reading password: %v", err), 1)
	}

	if string(pass) != string(pass2) {
		return cli.Exit(
			"\nThe passwords didn't match!", 1)
	}

	w.write(aurora.Bold("\nShould this new user be an editor? [y/N] "))
	s = bufio.NewScanner(os.Stdin)

	var isEditor bool
	s.Scan()
	switch strings.ToLower(strings.TrimSpace(s.Text())) {
	case "yes", "y":
		isEditor = true
	case "no", "n", "":
		isEditor = false
	default:
		return cli.Exit("You must enter yes or no.", 1)
	}

	w.write(aurora.Bold("Should this new user be an admin? [y/N] "))
	s = bufio.NewScanner(os.Stdin)

	var isAdmin bool
	s.Scan()
	switch strings.ToLower(strings.TrimSpace(s.Text())) {
	case "yes", "y":
		isAdmin = true
	case "no", "n", "":
		isAdmin = false
	default:
		return cli.Exit("You must enter yes or no.", 1)
	}

	var u *db.User
	errChan := make(chan error)
	doneChan := make(chan bool)

	go func() {
		u, err = f.DB.CreateUser(c.Context, name, pass, isAdmin, isEditor)
		if err != nil {
			errChan <- cli.Exit(
				fmt.Sprintf("Error creating user: %v", err), 1)
		}
		doneChan <- true
	}()

	ticker := time.NewTicker(100 * time.Millisecond)

out:
	for {
		select {
		case <-ticker.C:
			w.write(".")
		case err := <-errChan:
			return err
		case <-doneChan:
			break out
		}
	}

	w.write(aurora.Bold("\nSuccessfully created user!\n"))
	w.write(aurora.Bold("ID:"), aurora.Blue(u.ID), "\n")
	w.write(aurora.Bold("Username:"), aurora.Blue(u.Name), "\n")
	w.write(aurora.Bold("Is editor:"), aurora.Blue(u.IsEditor()), "\n")
	w.write(aurora.Bold("Is admin:"), aurora.Blue(u.IsAdmin()), "\n")

	return nil
}

type writer struct {
	io.Writer
}

func (w writer) write(args ...interface{}) {
	fmt.Fprint(w.Writer, args...)
}
