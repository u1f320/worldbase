package main

import (
	"log"
	"os"

	_ "github.com/joho/godotenv/autoload"
	"github.com/urfave/cli/v2"
	"gitlab.com/1f320/worldbase/cmd/user"
	"gitlab.com/1f320/worldbase/cmd/web"
	"gitlab.com/1f320/worldbase/common"
)

var app = &cli.App{
	Name:    "worldbase",
	Usage:   "Worldbuilding wiki software",
	Version: common.Version,
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:    "database",
			Aliases: []string{"d", "dir"},
			Usage:   "Load the database at `DIRECTORY`",
			Value:   "world.wbx",
			EnvVars: []string{"WORLDBASE_DB"},
		},
	},

	Commands: []*cli.Command{
		web.Command,
		user.Command,
	},
}

func main() {
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
