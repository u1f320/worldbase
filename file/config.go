package file

import (
	"os"
	"path/filepath"

	"emperror.dev/errors"
	"github.com/BurntSushi/toml"
	"gitlab.com/1f320/worldbase/db"
)

// Config is the configuration stored in `world.toml`
type Config struct {
	Name string `toml:"name,omitempty"`

	Prefs ConfigPreferences `toml:"prefs"`
	Data  ConfigData        `toml:"data"`
	Auth  ConfigAuth        `toml:"auth"`
}

// ConfigData is the static data stored in `world.toml`.
// Don't change this manually unless you want stuff to break!
type ConfigData struct {
	Database    string `toml:"database"`
	Version     int    `toml:"version"`
	RoutePrefix string `toml:"route_prefix"`
}

// ConfigPreferences ...
type ConfigPreferences struct {
	JSONLog  bool `toml:"json_log"`
	DebugLog bool `toml:"debug_log"`
}

// ConfigAuth ...
type ConfigAuth struct {
	Enabled  bool `toml:"enabled"`
	ReadOnly bool `toml:"read_only"`
	Private  bool `toml:"private"`
}

var defConfig = Config{
	Name: "Unnamed",
	Data: ConfigData{
		Database:    "data.db",
		Version:     db.SupportedVersion,
		RoutePrefix: "",
	},
}

// SaveConfig saves the current configuration to the config file.
func (f *File) SaveConfig() (err error) {
	f.mu.Lock()
	defer f.mu.Unlock()

	path := filepath.Join(f.Abs, ConfigName+".tmp")

	fi, err := os.CreateTemp(f.Abs, ConfigName)
	if err != nil {
		return errors.Wrap(err, "file.SaveConfig: create temp file")
	}

	enc := toml.NewEncoder(fi)
	enc.Indent = ""
	if err = enc.Encode(f.Config); err != nil {
		return errors.Wrap(err, "file.SaveConfig: encode config")
	}

	if err = fi.Sync(); err != nil {
		return errors.Wrap(err, "file.SaveConfig: sync config file")
	}

	if err = fi.Close(); err != nil {
		return errors.Wrap(err, "file.SaveConfig: close temp config file")
	}

	if err = os.Rename(path, filepath.Join(f.Abs, ConfigName)); err != nil {
		return errors.Wrap(err, "file.SaveConfig: overwrite config file")
	}
	return nil
}
