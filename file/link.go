package file

import (
	"fmt"
	"regexp"
	"strings"
)

var linkRegexp = regexp.MustCompile(`\[\[(.*?)(\|.*?)?\]\]`)

// LinkPages replaces all formatted links in the page with actual markdown links
func (f *File) LinkPages(input string) string {
	s := []string{}
	matches := linkRegexp.FindAllStringSubmatch(input, -1)

	for _, i := range matches {
		if len(i) < 3 {
			continue
		}

		input := i[1]

		if i[2] != "" {
			input = strings.TrimPrefix(i[2], "|")
		}

		page, err := f.Page(input)
		if err == nil {
			replace := fmt.Sprintf("[%v](%v/%v)", page.Name, f.Config.Data.RoutePrefix, page.Name)
			if len(i) > 2 {
				replace = fmt.Sprintf("[%v](%v/%v)", i[1], f.Config.Data.RoutePrefix, page.Name)
			}

			s = append(s, i[0], replace)
		}
	}

	return strings.NewReplacer(s...).Replace(input)
}
