package file

import (
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"emperror.dev/errors"
)

// doIndex (re)indexes all .md files in the repository.
// It assumes f.mu is already locked for writing.
func (f *File) doIndex() error {
	f.index = map[string]string{}

	err := filepath.WalkDir(f.RepoRoot, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return errors.Wrap(err, "ReadDir")
		}

		if d.IsDir() {
			if d.Name() == ".git" {
				return fs.SkipDir
			}

			return nil
		}

		if !strings.HasSuffix(d.Name(), ".md") {
			return nil
		}

		path = strings.Trim(strings.TrimPrefix(path, f.RepoRoot), "/")

		b, err := os.ReadFile(filepath.Join(f.RepoRoot, path))
		if err != nil {
			return err
		}

		f.index[strings.TrimSuffix(path, ".md")] = string(b)

		return nil
	})

	if err != nil {
		return errors.Wrap(err, "f.reindex: walk repository")
	}

	return nil
}

const ErrPageNotFound = errors.Sentinel("page not found in index")

func (f *File) Page(path string) (p Page, err error) {
	f.mu.RLock()
	defer f.mu.RUnlock()

	path = strings.Trim(path, "/")

	v, ok := f.index[path]
	if ok {
		return Page{
			Name:    path,
			Content: v,
		}, nil
	}

	return p, ErrPageNotFound
}
