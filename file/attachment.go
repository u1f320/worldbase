package file

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
	"path"

	"emperror.dev/errors"
	"gitlab.com/1f320/worldbase/db"
)

// UploadAttachment uploads an attachment.
func (f *File) UploadAttachment(ctx context.Context, name, contentType string, r io.ReadSeeker) (*db.File, error) {
	hasher := sha256.New()

	_, err := io.Copy(hasher, r)
	if err != nil {
		return nil, errors.Wrap(err, "f.UploadAttachment: copy to hash")
	}

	hash := hex.EncodeToString(hasher.Sum(nil))
	storedName := hash + path.Ext(name)

	fi, err := os.Create(path.Join(f.AttachmentDir, storedName))
	if err != nil {
		return nil, errors.Wrap(err, "f.UploadAttachment: create file")
	}
	defer fi.Close()

	_, err = r.Seek(0, io.SeekStart)
	if err != nil {
		return nil, errors.Wrap(err, "f.UploadAttachment: seek to start")
	}

	_, err = io.Copy(fi, r)
	if err != nil {
		return nil, errors.Wrap(err, "f.UploadAttachment: upload file")
	}

	return f.DB.AddFile(ctx, hash, name, storedName, contentType)
}
