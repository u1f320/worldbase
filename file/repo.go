package file

import (
	"log"
	"os"
	"path"

	"emperror.dev/errors"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/google/uuid"
	"gitlab.com/1f320/worldbase/db"
)

const ErrPathIsFile = errors.Sentinel("given path is a file")

// if force is true, rename an existing *file* to <path>.old
func verifyDirectoryExists(p string, force bool) error {
	fi, err := os.Stat(p)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return errors.Wrap(err, "stat directory")
		}

		log.Printf("Creating %v directory", p)

		err := os.MkdirAll(p, 0700)
		if err != nil {
			return errors.Wrap(err, "create directory")
		}

		_, err = os.Stat(p)
		if err != nil {
			return errors.Wrap(err, "stat directory")
		}

		log.Printf("Created %v directory", p)
	} else if !fi.IsDir() {
		if !force {
			return ErrPathIsFile
		}

		log.Printf("Warning: %v is not a directory. Renaming the file to \"%v.old\" and creating a new directory.", p, p)

		err = os.Rename(p, p+".old")
		if err != nil {
			return errors.Wrap(err, "rename file")
		}

		err = os.Mkdir(p, 0700)
		if err != nil {
			return errors.Wrap(err, "create directory")
		}
	}
	return nil
}

func (f *File) setupGit() error {
	workingDir := path.Join(f.RepoRoot)

	err := verifyDirectoryExists(workingDir, true)
	if err != nil {
		return errors.Wrap(err, "f.setupGit: verifying pages directory exists")
	}

	repo, err := git.PlainOpen(workingDir)
	if err != nil {
		if err != git.ErrRepositoryNotExists {
			return errors.Wrap(err, "f.setupGit: opening repository")
		}

		repo, err = git.PlainInit(workingDir, false)
		if err != nil {
			return errors.Wrap(err, "f.setupGit: creating repository")
		}
	}

	f.Repo = repo

	wt, err := f.Repo.Worktree()
	if err != nil {
		return errors.Wrap(err, "f.setupGit: getting worktree")
	}
	f.Worktree = wt

	_, err = f.Repo.Head()
	if err != nil {
		if err != plumbing.ErrReferenceNotFound {
			return errors.Wrap(err, "f.setupGit: getting HEAD")
		}

		return f.initialCommit()
	}

	return nil
}

func (f *File) initialCommit() error {
	for _, page := range defaultPages {
		fullPath := path.Join(f.RepoRoot, page.Name)
		dir := path.Dir(fullPath)

		err := verifyDirectoryExists(dir, true)
		if err != nil {
			return errors.Wrap(err, "f.initialCommit: verifying directory "+dir+" exists")
		}

		err = os.WriteFile(fullPath, page.Content, 0666)
		if err != nil {
			return errors.Wrap(err, "f.initialCommit: writing file "+page.Name)
		}
	}

	err := f.Worktree.AddGlob(".")
	if err != nil {
		return errors.Wrap(err, "f.initialCommit: staging files")
	}

	_, err = f.Worktree.Commit("Initial commit", &git.CommitOptions{
		Author: (db.User{
			ID:   uuid.Nil,
			Name: "system",
		}).Committer(),
	})
	if err != nil {
		return errors.Wrap(err, "f.initialCommit: creating commit")
	}

	return nil
}
