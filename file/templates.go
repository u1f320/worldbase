package file

import (
	"html/template"

	"github.com/Masterminds/sprig"
	"gitlab.com/1f320/worldbase/common"
)

func (f *File) Templates(tctx *TemplateContext) (*template.Template, error) {
	tmpl, err := template.New("").
		Funcs(sprig.FuncMap()).
		Funcs(tctx.Funcs()).
		ParseFS(common.TemplateData, "*.html")

	if err != nil {
		return nil, err
	}

	tctx.tmpl = tmpl
	return tmpl, nil
}

func (tctx *TemplateContext) Funcs() template.FuncMap {
	return template.FuncMap{
		"fullPath": func(path string) string {
			return tctx.FullPath(path)
		},
	}
}
