# Syntax

## Links

To create an internal link, use double brackets (`[[example]]`). To create a *named* internal link, separate the name and page title with a `|`, for example: `[[link name|page title]]`.
