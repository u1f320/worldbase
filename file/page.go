package file

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"emperror.dev/errors"
	"github.com/go-git/go-git/v5"
	"github.com/google/uuid"
	"gitlab.com/1f320/worldbase/db"
)

type InvalidPageNameError struct {
	Prefix string
	Suffix string
}

type Page struct {
	Name    string
	Content string
}

type SidebarEntry struct {
	Name  string `toml:"name"`
	Value string `toml:"value"`
}

func (e *InvalidPageNameError) Error() string {
	if e.Prefix != "" && e.Suffix != "" {
		return fmt.Sprintf("page name cannot start with %q and cannot end with %q", e.Prefix, e.Suffix)
	}

	if e.Prefix != "" {
		return fmt.Sprintf("page name cannot start with %q", e.Prefix)
	}

	if e.Suffix != "" {
		return fmt.Sprintf("page name cannot end with %q", e.Suffix)
	}

	return "invalid page name"
}

func (f *File) SavePage(name, content string, u db.User, reason string) error {
	if err := validatePageName(name); err != nil {
		return err
	}

	f.mu.Lock()
	defer f.mu.Unlock()

	path := filepath.Join(f.RepoRoot, name)

	err := verifyDirectoryExists(filepath.Dir(path), false)
	if err != nil {
		return errors.Wrap(err, "f.SavePage: directory")
	}

	err = os.WriteFile(path, []byte(content), 0666)
	if err != nil {
		return errors.Wrap(err, "f.SavePage: writing file")
	}

	_, err = f.Worktree.Add(path)
	if err != nil {
		return errors.Wrap(err, "f.SavePage: staging file")
	}

	if reason == "" {
		reason = "[no reason given]"
	}

	_, err = f.Worktree.Commit(reason, &git.CommitOptions{
		Author: u.Committer(),
	})
	if err != nil {
		return errors.Wrap(err, "f.SavePage: creating commit")
	}

	err = f.doIndex()
	if err != nil {
		return errors.Wrap(err, "f.SavePage: reindexing repository")
	}

	return nil
}

func validatePageName(name string) error {
	name = strings.ToLower(name)
	if strings.HasPrefix(name, "auth") {
		return &InvalidPageNameError{Prefix: "auth"}
	} else if strings.HasPrefix(name, "static") {
		return &InvalidPageNameError{Prefix: "static"}
	} else if strings.HasPrefix(name, "search") {
		return &InvalidPageNameError{Prefix: "search"}
	} else if strings.HasPrefix(name, "list") {
		return &InvalidPageNameError{Prefix: "list"}
	} else if strings.HasPrefix(name, "revision") {
		return &InvalidPageNameError{Prefix: "revision"}
	} else if strings.HasPrefix(name, "edit") {
		return &InvalidPageNameError{Prefix: "edit"}
	} else if strings.HasPrefix(name, "history") {
		return &InvalidPageNameError{Prefix: "history"}
	}
	return nil
}

type PageMetadata struct {
	LastAuthor   string
	LastAuthorID uuid.UUID
	LastModified time.Time
	Diff         string
}

func (md PageMetadata) Time() string {
	return md.LastModified.Format("January 2 2006 at 03:04:05 PM")
}

func (f *File) PageMetadata(ctx context.Context, path string) (md PageMetadata, err error) {
	commits, err := f.Repo.Log(&git.LogOptions{
		PathFilter: func(s string) bool {
			return s == path+".md"
		},
	})
	if err != nil {
		return md, errors.Wrap(err, "f.PageMetadata: fetch log")
	}

	commit, err := commits.Next()
	if err != nil {
		return md, errors.Wrap(err, "f.PageMetadata: commits.Next")
	}

	md = PageMetadata{
		LastAuthor:   commit.Author.Name,
		LastModified: commit.Author.When,
	}

	if rawUUID, _, ok := strings.Cut(commit.Author.Email, "@"); ok {
		md.LastAuthorID, err = uuid.Parse(rawUUID)
		if err != nil {
			md.LastAuthorID = uuid.Nil
		}
	} else {
		md.LastAuthorID = uuid.Nil
	}

	parent, err := commit.Parent(0)
	if err != nil {
		return md, nil
	}

	patch, err := parent.PatchContext(ctx, commit)
	if err != nil {
		return md, errors.Wrap(err, "f.PageMetadata: parent.PatchContext")
	}

	b := new(bytes.Buffer)
	err = patch.Encode(b)
	if err != nil {
		return md, errors.Wrap(err, "f.PageMetadata: patch.Encode")
	}
	md.Diff = b.String()

	return md, nil
}
