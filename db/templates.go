package db

import (
	"context"
	"time"

	sq "github.com/Masterminds/squirrel"
)

// Template is a template stored in the database.
type Template struct {
	ID      int64     `db:"id"`
	Name    string    `db:"name"`
	Content string    `db:"content"`
	Created time.Time `db:"created"`
}

var templates = struct {
	Table             string
	ID, Name, Content string
	Created           string
}{"templates", "id", "name", "content", "created"}

// Templates returns all templates in the database, sorted by ID.
func (db *DB) Templates(ctx context.Context) (ts []Template, err error) {
	sql, _, err := sq.Select("*").From(templates.Table).OrderBy(templates.ID).ToSql()
	if err != nil {
		return nil, err
	}

	err = db.SelectContext(ctx, &ts, sql)
	return ts, err
}

// CreateTemplate creates a template with the given name and returns it.
func (db *DB) CreateTemplate(ctx context.Context, name, content string) (*Template, error) {
	var t Template

	sql, args, err := sq.Insert(templates.Table).Columns(templates.Name, templates.Content).Values(name, content).Suffix("RETURNING *").ToSql()
	if err != nil {
		return nil, err
	}

	err = db.GetContext(ctx, &t, sql, args...)
	if err != nil {
		return nil, err
	}
	return &t, nil
}
