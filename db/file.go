package db

import (
	"context"

	sq "github.com/Masterminds/squirrel"
)

// File is a single file stored in the database
type File struct {
	Hash        string `db:"id"`
	Name        string `db:"name"`
	StoredName  string `db:"stored_name"`
	ContentType string `db:"content_type"`
}

var files = struct {
	Table                               string
	Hash, Name, StoredName, ContentType string
}{"files", "hash", "name", "stored_name", "content_type"}

// AddFile adds a file
func (db *DB) AddFile(ctx context.Context, hash, filename, storedName, contentType string) (*File, error) {
	var f File

	sql, args, err := sq.Insert(files.Table).Columns(files.Hash, files.Name, files.StoredName, files.ContentType).Values(hash, filename, storedName, contentType).Suffix("RETURNING *").ToSql()
	if err != nil {
		return nil, err
	}

	err = db.GetContext(ctx, &f, sql, args...)
	if err != nil {
		return nil, err
	}
	return &f, err
}
