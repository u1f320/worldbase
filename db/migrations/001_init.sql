-- +migrate Up

CREATE TABLE users (
    id          TEXT    PRIMARY KEY NOT NULL,
    name        TEXT    NOT NULL    UNIQUE,
    password    BLOB    NOT NULL,
    salt        TEXT    NOT NULL,
    -- bitmask of flags
    flags       INTEGER NOT NULL    DEFAULT 0,

    -- preferences
    dark_mode   BOOLEAN NOT NULL    DEFAULT 0
);

CREATE TABLE sessions (
    user_id     TEXT     NOT NULL    REFERENCES users (id) ON DELETE CASCADE,
    session_id  TEXT     NOT NULL    UNIQUE,
    cookie      TEXT     PRIMARY KEY NOT NULL,
    expires     DATETIME NOT NULL
);

CREATE TABLE invites (
    code    TEXT    NOT NULL    PRIMARY KEY,
    inviter TEXT    NOT NULL    REFERENCES users (id) ON DELETE CASCADE,
    user_id TEXT    REFERENCES users (id) ON DELETE SET NULL,
    claimed BOOLEAN NOT NULL    DEFAULT 0,
    created INTEGER NOT NULL    DEFAULT (strftime('%s', 'now'))
);

CREATE TABLE files (
    hash            TEXT    PRIMARY KEY,
    name            TEXT    NOT NULL,
    user_id         TEXT    REFERENCES users (id) ON DELETE SET NULL,
    stored_name     TEXT    NOT NULL    UNIQUE,
    content_type    TEXT    NOT NULL,
    created         INTEGER NOT NULL    DEFAULT (strftime('%s', 'now'))
);

CREATE TABLE templates (
    id      INTEGER PRIMARY KEY,
    name    TEXT    NOT NULL    UNIQUE,
    user_id TEXT    REFERENCES users (id) ON DELETE SET NULL,
    content TEXT    NOT NULL,
    created INTEGER NOT NULL    DEFAULT (strftime('%s', 'now'))
);
