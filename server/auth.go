package server

import (
	"net/http"

	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/db"
)

func (r *Router) MaybeAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		cookie, err := req.Cookie(common.SessionCookieName)
		if err != nil {
			next.ServeHTTP(w, req)
			return
		}

		u, err := r.File.DB.UserFromCookie(ctx, cookie.Value)
		if err != nil {
			if err != db.ErrUserNotFound {
				common.Log.Errorf("Error getting user from databsae: %v", err)
			}

			next.ServeHTTP(w, req)
			return
		}

		next.ServeHTTP(w, req.WithContext(ContextWithUser(ctx, u)))
	})
}
