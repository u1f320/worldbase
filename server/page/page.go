package page

import (
	"html/template"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/russross/blackfriday/v2"
	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/file"
	"gitlab.com/1f320/worldbase/server"
)

// Router is the page router.
type Router struct {
	*server.Router
}

// Mount mounts this router.
func Mount(s *server.Router) {
	r := &Router{s}

	// redirect to /w/index
	r.Get("/", r.redirectRoot)
	// get all other pages
	r.HandleFunc("/*", r.getPage)
	r.HandleFunc("/edit/*", nil)
	r.HandleFunc("/history/*", nil)
}

func (r *Router) redirectRoot(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req, r.Redirect("/index"), http.StatusMovedPermanently)
}

func (r *Router) getPage(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	if action := req.FormValue("action"); action != "" {
		path := chi.URLParam(req, "*")
		var err error
		switch action {
		case "edit":
			err = r.editPage(w, req, path)
		}
		if err != nil {
			common.Log.Errorf("Error executing action %q for %q: %v", action, path, err)
			http.Redirect(w, req, r.Redirect("/index?error=500"), http.StatusTemporaryRedirect)
			return
		}
	}

	tctx, err := r.File.TemplateContext("page")
	if err != nil {
		common.Log.Errorf("Error getting template context for page: %v", err)
		return
	}

	u, ok := server.UserFromContext(ctx)
	if ok {
		tctx.User = &u
	} else {
		if r.File.Config.Auth.Private {
			http.Redirect(w, req, r.Redirect("/auth/login"), http.StatusTemporaryRedirect)
			return
		}
	}

	path := chi.URLParam(req, "*")

	page, err := r.File.Page(path)
	if err != nil {
		http.Redirect(w, req, r.Redirect("/index?error=404"), http.StatusTemporaryRedirect)
		return
	}

	tctx.Title = firstHeader(page.Name, []byte(page.Content))
	if page.Name == "index" {
		tctx.Active = "index"
	}

	pctx := newPageContext(tctx)
	pctx.Path = page.Name
	pctx.Content = page.Content
	pctx.ParsedContent = pctx.tctx.RenderPageMarkdown(page.Content)

	// collect metadata
	pctx.Metadata, err = r.File.PageMetadata(ctx, path)
	if err != nil {
		common.Log.Errorf("Error getting page metadata for %q: %v", path, err)
		http.Redirect(w, req, r.Redirect("/index?error=500"), http.StatusTemporaryRedirect)
		return
	}

	errValue(req, tctx)

	err = tctx.ExecTemplate(w, pctx)
	if err != nil {
		common.Log.Errorf("Error executing page template: %v", err)
	}
}

func errValue(req *http.Request, tctx *file.TemplateContext) {
	switch req.FormValue("error") {
	case "404":
		tctx.AlertType = "danger"
		tctx.Alert = "Page not found."
	case "500":
		tctx.AlertType = "danger"
		tctx.Alert = "Internal server error."
	case "already-logged-out":
		tctx.AlertType = "danger"
		tctx.Alert = "You are already logged out."
	case "already-logged-in":
		tctx.AlertType = "danger"
		tctx.Alert = "You are already logged in."
	case "successful-login":
		tctx.AlertType = "success"
		tctx.Alert = "Successfully logged in!"
	case "successfully-logged-out":
		tctx.AlertType = "success"
		tctx.Alert = "Succesfully logged out!"
	}
}

func firstHeader(path string, content []byte) string {
	node := blackfriday.New().Parse(content)

	name := ""

	node.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
		if !entering {
			return blackfriday.GoToNext
		}

		if node.Type == blackfriday.Heading {
			if node.FirstChild != nil {
				name = string(node.FirstChild.Literal)
			}

			return blackfriday.Terminate
		}

		return blackfriday.GoToNext
	})

	if name == "" {
		segments := strings.Split(path, "/")
		if len(segments) == 0 {
			return path
		}

		name = segments[len(segments)-1]
	}

	return name
}

type pageContext struct {
	tctx *file.TemplateContext

	Path, Content string
	ParsedContent template.HTML
	Metadata      file.PageMetadata
}

func (ctx *pageContext) EditPath() string {
	return ctx.tctx.FullPath(ctx.Path + "?action=edit")
}

func (ctx *pageContext) HistoryPath() string {
	return ctx.tctx.FullPath(ctx.Path + "?action=history")
}

func (ctx *pageContext) FullPath() string {
	return ctx.tctx.FullPath(ctx.Path)
}

func (ctx *pageContext) IsEditor() bool {
	if ctx.tctx.User == nil {
		return false
	}

	return ctx.tctx.User.IsEditor() || ctx.tctx.User.IsAdmin()
}

func newPageContext(tctx *file.TemplateContext) *pageContext {
	return &pageContext{
		tctx: tctx,
	}
}
