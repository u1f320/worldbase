package page

import (
	"net/http"

	"emperror.dev/errors"
	"gitlab.com/1f320/worldbase/server"
)

func (r *Router) editPage(w http.ResponseWriter, req *http.Request, path string) error {
	ctx := req.Context()

	tctx, err := r.File.TemplateContext("edit")
	if err != nil {
		return errors.Wrap(err, "getting template context")
	}

	u, ok := server.UserFromContext(ctx)
	if ok {
		tctx.User = &u
	} else {
		http.Redirect(w, req, r.Redirect("/auth/login"), http.StatusTemporaryRedirect)
		return nil
	}

	page, err := r.File.Page(path)
	if err != nil {
		http.Redirect(w, req, r.Redirect("/index?error=404"), http.StatusTemporaryRedirect)
		return err
	}
	_ = page

	return nil
}
