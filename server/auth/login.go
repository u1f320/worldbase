package auth

import (
	"net/http"
	"time"

	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/db"
	"gitlab.com/1f320/worldbase/file"
	"gitlab.com/1f320/worldbase/server"
	"golang.org/x/crypto/bcrypt"
)

func (r *Router) getLogin(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	_, ok := server.UserFromContext(ctx)
	if ok {
		// can't log in again if you're already logged in
		http.Redirect(w, req, r.Redirect("/index?error=already-logged-in"), http.StatusTemporaryRedirect)
		return
	}

	// clear any existing cookies just in case
	http.SetCookie(w, &http.Cookie{
		Name:    common.SessionCookieName,
		Value:   "",
		Expires: time.Now(),
		Path:    "/",
	})

	tctx, err := r.File.TemplateContext("login")
	if err != nil {
		common.Log.Errorf("Error creating login template context: %v", err)
		return
	}

	tctx.Title = "Log in"

	errValue(req, tctx)

	err = tctx.ExecTemplate(w, nil)
	if err != nil {
		common.Log.Errorf("Error executing login template: %v", err)
	}
}

func errValue(req *http.Request, tctx *file.TemplateContext) {
	switch req.FormValue("error") {
	case "invalid-login":
		tctx.AlertType = "danger"
		tctx.Alert = "Invalid username or password."
	case "unknown-error":
		tctx.AlertType = "danger"
		tctx.Alert = "An unknown error occurred."
	case "not-logged-in":
		tctx.AlertType = "danger"
		tctx.Alert = "You must be logged in to access this page."
	}
}

func (r *Router) postLogin(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	username := req.FormValue("username")
	password := req.FormValue("password")

	u, err := r.File.DB.User(ctx, username)
	if err != nil {
		if err != db.ErrUserNotFound {
			common.Log.Errorf("Error getting user %q: %v", username, err)
		}

		http.Redirect(w, req, r.Redirect("/auth/login?error=invalid-login"), http.StatusTemporaryRedirect)
		return
	}

	err = u.CheckPassword(password)
	if err != nil {
		if err != bcrypt.ErrMismatchedHashAndPassword {
			common.Log.Errorf("Error checking password hash for user %q: %v", username, err)
		}

		http.Redirect(w, req, r.Redirect("/auth/login?error=invalid-login"), http.StatusTemporaryRedirect)
		return
	}

	// everything checks out
	cookie, err := r.File.DB.CreateUserCookie(ctx, *u)
	if err != nil {
		common.Log.Errorf("Error storing cookie for user %q: %v", username, err)

		http.Redirect(w, req, r.Redirect("/auth/login?error=unknown-error"), http.StatusTemporaryRedirect)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    common.SessionCookieName,
		Value:   cookie,
		Path:    "/",
		Expires: time.Now().Add(db.CookieExpiry),
	})

	http.Redirect(
		w, req, r.Redirect("/index?error=successful-login"), http.StatusTemporaryRedirect,
	)
}
