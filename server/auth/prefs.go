package auth

import (
	"net/http"

	"gitlab.com/1f320/worldbase/common"
	"gitlab.com/1f320/worldbase/db"
	"gitlab.com/1f320/worldbase/server"
)

func (r *Router) getPreferences(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	u, ok := server.UserFromContext(ctx)
	if !ok {
		http.Redirect(w, req,
			r.Redirect("/auth/login?error=not-logged-in"), http.StatusTemporaryRedirect,
		)
		return
	}

	tctx, err := r.File.TemplateContext("preferences")
	if err != nil {
		common.Log.Errorf("Error creating template context: %v", err)
		http.Redirect(w, req, r.Redirect("/index?error=500"), http.StatusTemporaryRedirect)
		return
	}

	tctx.User = &u
	tctx.Title = "Preferences"
	tctx.Active = "preferences"

	var pctx struct {
		User db.User
	}
	pctx.User = u

	err = tctx.ExecTemplate(w, pctx)
	if err != nil {
		common.Log.Errorf("Error executing preferences template: %v", err)
	}
}

func (r *Router) getActiveSessions(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	u, ok := server.UserFromContext(ctx)
	if !ok {
		http.Redirect(w, req,
			r.Redirect("/auth/login?error=not-logged-in"), http.StatusTemporaryRedirect,
		)
		return
	}

	tctx, err := r.File.TemplateContext("preferences_sessions")
	if err != nil {
		common.Log.Errorf("Error creating template context: %v", err)
		http.Redirect(w, req, r.Redirect("/index?error=500"), http.StatusTemporaryRedirect)
		return
	}
	tctx.User = &u
	tctx.Title = "Active sessions"
	tctx.Active = "preferences"

	var pctx struct {
		Sessions      []db.Session
		CurrentCookie string
	}

	pctx.Sessions, err = r.File.DB.UserSessions(ctx, u.ID)
	if err != nil {
		common.Log.Errorf("Error getting user sessions: %v", err)
		http.Redirect(w, req, r.Redirect("/index?error=500"), http.StatusTemporaryRedirect)
		return
	}

	cookie, err := req.Cookie(common.SessionCookieName)
	if err == nil {
		pctx.CurrentCookie = cookie.Value
	}

	err = tctx.ExecTemplate(w, pctx)
	if err != nil {
		common.Log.Errorf("Error executing sessions template: %v", err)
	}
}
