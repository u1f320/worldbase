package common

import (
	"crypto/rand"
	"embed"
	"encoding/base64"
	"io/fs"
	"log"
	"net/http"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const SessionCookieName = "worldbase-session"

// Version is the current Git commit hash
var Version = "[unknown]"

// Log is the global logger
var Log = zap.S()

// SetupLog sets up the logger
func SetupLog(json, debug bool) {
	zcfg := zap.NewProductionConfig()

	if !json {
		zcfg.Encoding = "console"
		zcfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		zcfg.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		zcfg.EncoderConfig.EncodeCaller = zapcore.ShortCallerEncoder
		zcfg.EncoderConfig.EncodeDuration = zapcore.StringDurationEncoder
	}

	if debug {
		zcfg.Level.SetLevel(zapcore.DebugLevel)
	} else {
		zcfg.Level.SetLevel(zapcore.InfoLevel)
	}

	logger, err := zcfg.Build(zap.AddStacktrace(zapcore.ErrorLevel))
	if err != nil {
		log.Fatalf("Could not build logger: %v", err)
	}

	zap.RedirectStdLog(logger)

	Log = logger.Sugar()
}

//go:embed data
var static embed.FS

func mustSub(f fs.FS, path string) fs.FS {
	sub, err := fs.Sub(f, path)
	if err != nil {
		panic(err)
	}
	return sub
}

var StaticServer = http.StripPrefix("/static/", http.FileServer(http.FS(mustSub(static, "data/static"))))

var TemplateData = mustSub(static, "data/templates")

func RandBase64(size int) string {
	b := make([]byte, size)

	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString(b)
}
